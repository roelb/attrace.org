module.exports = {
  docs: {
    Documentation: [
      'docs',
      'roots',
      'networks',
    ],
    Connectors: ['connectors/connectors'],
    Guides: [
      'guides/cli-client',
      'guides/token-rates',
    ],
    API: [
      'api/witness-http', 
      'api/index-http',
    ],
    Nodes: [
      'nodes/nodes',
      'nodes/node-configuration',
      'nodes/index-node-setup',
      'nodes/public-node-setup',
      'nodes/witness',
    ],
    Engineering: [
      'engineering/mindset',
      'engineering/builds',
    ]
  },
};
