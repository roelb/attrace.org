module.exports = {
  title: 'Attrace open blockchain network',
  tagline: 'Attrace is a decentralized global platform for affiliate functionality',
  // url: 'https://attrace.org', // LATER
  url: 'https://attrace-org-docs.netlify.app', // Url to your site with no trailing slash
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: 'attrace', // Usually your GitHub org/user name.
  projectName: 'attrace.org', // Usually your repo name.
  customFields: {
    baseEditUrl: 'https://gitlab.com/attrace/attrace.org/-/blob/develop/',
  },
  themeConfig: {
    sidebarCollapsible: false,
    colorMode: {
      respectPrefersColorScheme: true,
    },
    navbar: {
      title: 'Attrace Docs',
      hideOnScroll: false,
      logo: {
        alt: 'Attrace logo',
        // TODO logo should be color diff from attrace.com -> would go grey/green --- green = project = .org, blue = implementation = .com
        src: 'img/logo.png', 
      },
      items: [
        {
          to: 'intro/',
          activeBasePath: 'intro',
          label: 'Introduction',
          position: 'left',
        },
        {
          to: 'software/',
          activeBasePath: 'software',
          label: 'Software',
          position: 'left',
        },
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          label: 'API',
          position: 'left',
          items: [
            {
              to: 'docs/api/witness-http',
              label: 'Witness API',
            },
            {
              to: 'docs/api/index-http',
              label: 'Index API',
            },
          ]
        },
        {
          // TODO: move these components to this repo and shut down dev-tools.attrace.com.
          to: 'contracts',
          label: 'Contracts',
          position: 'left',
        },
        {
          // TODO: move these components to this repo and shut down dev-tools.attrace.com.
          href: 'https://dev-tools.attrace.com',
          label: 'Dev Tools',
          position: 'left',
        },
        {
          href: 'https://attrace.com',
          label: 'attrace.com',
          position: 'right',
        },
        // {
        //   to: 'docs/',
        //   activeBasePath: 'docs',
        //   label: 'Docs',
        //   position: 'left',
        // },
        // -- KEEP AS EXAMPLE REFS
        // {
        //   to: 'docs/',
        //   activeBasePath: 'docs',
        //   label: 'Docs',
        //   position: 'left',
        // },
        // {to: 'blog', label: 'Blog', position: 'left'},
        // {
        //   href: 'https://github.com/facebook/docusaurus',
        //   label: 'GitHub',
        //   position: 'right',
        // },
      ],
    },
    footer: {
      style: 'dark',
      // links: [
      //   // {
      //   //   title: 'Docs',
      //   //   items: [
      //   //     {
      //   //       label: 'Style Guide',
      //   //       to: 'docs/',
      //   //     },
      //   //     {
      //   //       label: 'Second Doc',
      //   //       to: 'docs/doc2/',
      //   //     },
      //   //   ],
      //   // },
      //   {
      //     title: 'Community',
      //     items: [
      //       {
      //         label: 'Telegram',
      //         href: 'https://t.me/attrace',
      //       },
      //     ],
      //   },
      //   {
      //     title: 'More',
      //     items: [
      //       {
      //         label: 'GitLab',
      //         href: 'https://gitlab.com/attrace/attrace.org',
      //       },
      //       {
      //         label: 'Attrace.com',
      //         href: 'https://attrace.com',
      //       },
      //     ],
      //   },
      // ],
      copyright: `You are reading the Attrace technical docs.<br/>Visit <a href="https://attrace.com">attrace.com</a> instead if you are looking for the live affiliate network (which runs on the Attrace blockchain). <br /><br/>Copyright © ${new Date().getFullYear()} Attrace, BV.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/attrace/attrace.org/-/blob/develop/',
          lastVersion: 'current',
        },
        // blog: {
        //   showReadingTime: true,
        //   // Please change this to your repo.
        //   editUrl:
        //     'https://github.com/facebook/docusaurus/edit/develop/website/blog/',
        // },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],

  plugins: [
    // software
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'software',
        path: 'software',
        routeBasePath: 'software',
        editUrl: 'https://gitlab.com/attrace/attrace.org/-/blob/develop/',
        includeCurrentVersion: true,
        lastVersion: 'current',
      },
    ],

    // intro
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'intro',
        path: 'intro',
        routeBasePath: 'intro',
        editUrl: 'https://gitlab.com/attrace/attrace.org/-/blob/develop/',
        includeCurrentVersion: true,
        lastVersion: 'current',
      },
    ],
  ],
};
