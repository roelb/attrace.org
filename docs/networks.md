---
title: Networks
hide_table_of_contents: true
---

Different networks are active.

| Environment | Description                                                                          | Managed by    | Genesis Block                                        |
|-------------|--------------------------------------------------------------------------------------|---------------|------------------------------------------------------|
| betanet     | Network which will receive live traffic and will evolve into live production network | Token Holders | NJV4Y2MEVZSZSYCTMORA53ZUB6SRTYE2VW6IB3GT3DLG3TJK3V5A |
| testnet     | Online testnet which can break due to developer changes                              | Attrace Team  | 6PYHXBHFOTQZ3GE6JQRA2OYDCU6WTLHLF3JID6JZI6TRKUMWEYPQ |
| devnet      | Development network developers run locally during development                                | Developer     | see code                                             |

## Discovery manifest

When booting a new node, it's required to give the node some hints how to discover the network. It needs at least a single witness to start discovery.

When you use any of the clients, by default they will discover the requested network by querying for a manifest.   
The Attrace Project, hosts an up-to-date witness manifest for every network. It refreshes every 15 minutes.

This is the url pattern for the manifest:

```
https://s3.eu-central-1.amazonaws.com/discovery.attrace.com/{{ network }}/manifest.json
```

Example:

```
curl -s https://s3.eu-central-1.amazonaws.com/discovery.attrace.com/betanet/manifest.json | jq
{
  "SnapshotTime": 1578953766399,
  "Witnesses": [
    {
      "Address": "ABHHCJYIAPY323PLPEJYLA2E6XH242AJ25FYYJPST3ANCCQ2FB7AVEA5",
      "ApiPort": 0,
      "Host": "node-sgiylh03.betanet.attrace.com",
      "Port": 443,
      "Signature": "ZVBJ2mo797W9xcfNPLNJ/3nctVJ6jzwYShluBWTW0H7CN8NKxSNuxb7mlhJyeSgsCqXZkbz523cEqpuUQvD7CA==",
      "Time": 1578953673105
    },
    ...
  ]
}
```

### Custom manifest
If you prefer your run your own public node, or want to use a custom manifest, you can always pass in `--discovery-manifest=URL` on all executables.   

Every public node exposes the manifest on `GET /api/v1/manifests/network`.

**Example request:**
```
curl 'https://YOURNODE/api/v1/manifests/network' | jq
```

### Custom seeds
If you don't want to use a manifest, you can also use `--seeds=host:port,,...` to discover from specific hosts.
