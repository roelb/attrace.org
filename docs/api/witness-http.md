---
title: Witness HTTP API
sidebar_label: Witness Node API
---

## /api/v1/blockchain/address/{AddressHash}
Get the public blockchain state of an account or agreement.

 - Account: Return token values
 - Agreement: Return agreement state: parties, confirmed parties, contract hash, ...

**Parameters**:

 - Hash: Address of account or agreement as base 32 encoded

**Swagger UI**:

-  [`/api-docs/#/Full/GetAddressStates`](https://node-sgiylh03.betanet.attrace.com/api-docs/#/Full/GetAddressStates)

**Example**:
```
curl -X GET \
  'https://peer-7gb9gln301.testnet.attrace.com/api/v1/blockchain/address/AA7YZSFEXR446J3QYCSIAKE6TNIHUWXUE2T575WXRSS4VZZAJQ2PAK6O'
``` 

**Response**:

Account:
```json
{
  "AddressStates": [
    {
      "Account": {
        "Value": "9999999996992600",
        "TokenLeases": [
          {
            "Provider": "AAFLTUOGS6GUIABD3KDSB6MYE6JICYR2RU6HS7SLLRHUTMUJGX2I7BMG",
            "RemainingValue": "1999999999700"
          }
        ]
      }
    }
  ]
}
```

Agreement:
```json
{
  "AddressStates": [
    {
      "Agreement": {
        "ConfirmedParties": [
          0,
          1
        ],
        "CreateOperation": {
          "ActionParties": [
            0,
            1
          ],
          "ChannelAccess": [
            0,
            1
          ],
          "ContractHash": "FJR4YIFH2G5J7CWG6GXRMOSIV3H5M62OGM7ACGNTGXIRMYVP36AQ",
          "HashNonce": "6njohVwhsohf5FlBlp2eM66V8PHZ/h2RBgKFFgxlUn0=",
          "Parties": [
            "AA7YZSFEXR446J3QYCSIAKE6TNIHUWXUE2T575WXRSS4VZZAJQ2PAK6O",
            "ADBTNR23CCGEVXKFNWNN26TYKZGWEOXPLUBLTWOUFKMXIFKLJPT3CLVX"
          ],
          "RootCreators": [
            0,
            1
          ]
        },
        "Parties": [
          "AA7YZSFEXR446J3QYCSIAKE6TNIHUWXUE2T575WXRSS4VZZAJQ2PAK6O",
          "ADBTNR23CCGEVXKFNWNN26TYKZGWEOXPLUBLTWOUFKMXIFKLJPT3CLVX"
        ],
        "Value": "100"
      }
    }
  ]
}
```

## /api/v1/accounts/{AccountAddress}/block-hashes
Get all the block hashes for blocks where an account has referenced in any of the block's transactions.

This is the entrypoint to browse the blockchain by account. Using this information it's possible to fetch blocks and to parse them for related transactions.

**Parameters**:

 - AccountAddress: Address of account base 32 encoded
 - Limit: uint64 number, maximum number of hashes returned
 - GetLatest: boolean, default is **false**. If true, would return the latest block headers only. Ignore the *FromBlock* and *ToBlock*
 - FromBlock/ToBlock: Optional, only applicable if *GetLatest=false*. Define range of block headers we want to query,

**Swagger UI**:

-  [`/api-docs/#/Full/GetAccountBlockHashes`](https://node-sgiylh03.betanet.attrace.com/api-docs/#/Full/GetAccountBlockHashes)

**Example**:
```
curl -X GET \
  'https://node-sgiylh03.betanet.attrace.com/api/v1/accounts/AB5KTGT27ZEBT7UYQ3KVHV5WLF4MZY65DXUFSSFDFXGDHIMXRQ37E7V7/block-hashes?Limit=1000&GetLatest=true'
``` 

**Response**:
```json
{
  "BlockHashes": [
    "NJV4Y2MEVZSZSYCTMORA53ZUB6SRTYE2VW6IB3GT3DLG3TJK3V5A"
  ]
}
```

## /api/v1/blocks-raw
Get binary blocks from the public chain by block hashes. They are returned encoded in the wire format (protobuf) and can be decoded using any of the client libraries.

**Parameters**:

 - Hashes: The hashes of the blocks you want to retrieve. Repeat the `Hashes=` param for multiple, eg: `?Hashes=BlockHash1&Hashes=BlockHash2&Hashes=BlockHash3&...`

**Swagger UI**:

-  [`/api-docs/#/Full/GetRawBlocksByHashes`](https://node-sgiylh03.betanet.attrace.com/api-docs/#/Full/GetRawBlocksByHashes)

**Example**:
```
curl -X GET \
  'https://node-sgiylh03.betanet.attrace.com/api/v1/blocks-raw?Hashes=VY2KROL5T6T3PIFOWIEMUO6J32KGL3L2IXU6WF7BCJ4ME7TNSDDA&Hashes=QCTM4BRQQON33PZSFQIPGAOXRDABCQE5GFNGD3R47WO6TMNKNFDQ'
``` 

**Response**:

Response is streamed in line-delimited JSON format, having the protobuf binaries encoded using base64 in the `result.Data` structure.   
You can parse in a streaming approach as well.

```json
{"result":{"Data":"CrYBCNjMGxCF4oSx/C0aIwD0Q1lb6ULyk14C4dM77wt0BjyGhVnKYsCiuV3GA+lEkg8NIiCuNKi5fZ+nt6CusgjKO8nelGXtekXp6xfhEnjCfm2QxiogpvYtEbXJZBZJlx8DJWchEuM9R4Rb83DZiYo0ez1ThMU6QNWIrDS2Zo2rhk3NB4dw+WPAob33vPIuCLh42oMDRQTLk/y24e+76OJgCOYP7n7cmV55qOeJVNZu25f/GlV8eA4="}}
{"result":{"Data":"CrYBCNnMGxCX54Sx/C0aIwD0Q1lb6ULyk14C4dM77wt0BjyGhVnKYsCiuV3GA+lEkg8NIiCAps4GMIObvb8yLBDzAdeIwBFAnTFaYe48/Z3psappRyogrjSouX2fp7egrrIIyjvJ3pRl7XpF6esX4RJ4wn5tkMY6QO18rTz0WuuUuwxEs220DqHGGH201ciF5O3wHoecSiABI3cd/gX3JhFaieerMsVKbzQF2aYsDHCpQYznu4lXbA0="}}
```
