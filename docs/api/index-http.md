---
title: Index node HTTP API
sidebar_label: Index Node API
---

## /api/v1/operation-history
Operation call history grouped by period.

Supports grouping by: day

**Parameters**:
 - `Agreements`: The array of base 32 encoded addresses of the agreement you want to filter the actions
 - `FromTime` Start of time range to filter the compensations. Timestamp in milliseconds (UTC).
 - `ToTime`: End of the time range to filter the compensations. Timestamp in milliseconds (UTC).
 - `Timezone`: Timezone for filter, used to calculate time when filter on specific timezone. This has to be a valid value according to https://golang.org/pkg/time/#LoadLocation . Default is empty (UTC).
 - `TimeAggregation`: What time grouping. Available: day.
 - `Operations`: Optional, list of operation types to include. Eg: root, actioncall.

 **Example**:
```
curl --request GET \
  --url 'https://node.lonet:9179/api/v1/operation-history?Agreements=ARJUFUYN4G625YLWWS6BDHXVWNKKQ6PMJWL6C35EE5SHDKY5VWB7OPE4&FromTime=1585967899000&ToTime=1684967899000&TimeAggregation=day'
```

**Response**:

```json
{
  "Dimensions": [
    {
      "Field": "date"
    },
    {
      "Field": "action"
    }
  ],
  "Metrics": [
    {
      "Field": "count",
      "DataType": "int64"
    }
  ],
  "Results": [
    {
      "Values": [
        "2020-04-06",
        "click",
        "3"
      ]
    },
    {
      "Values": [
        "2020-04-06",
        "approved",
        "2"
      ]
    }
  ]
}
```

## /api/v1/operation/stream
Websocket handler to streaming latest operation

**Parameters**:
 - `Agreements`: The array of base 32 encoded addresses of the agreement you want to filter the actions.

**Example**:
```
curl --request GET \
    --url https://node.lonet:9179/api/v1/operation/stream?Agreements=ARJUFUYN4G625YLWWS6BDHXVWNKKQ6PMJWL6C35EE5SHDKY5VWB7OPE4
```

**Response**:
```json
{
  "result": {
    "Sequence": "1",
    "Operations": [
      {
        "Agreement": "ARJUFUYN4G625YLWWS6BDHXVWNKKQ6PMJWL6C35EE5SHDKY5VWB7OPE4",
        "Time": "2020-04-12T18:49:12Z",
        "Operation": "actioncall",
        "Action": "approved",
        "Tx": "LSEU62SQND7KFTHCJJFMT65ZUMJGDIOVO23PH5B3T7WZET57ED2A",
        "Root": "BACA6ZL7KKNDVJJ764WAAQ2UN5I5WSDV23H7DJAAXDF3C3MQHJSNV5U2",
        "Account": "ACPK4HPCM6HAAPAIE75ZJFSMNK2UI56CTCAAXNKI4THIQ33OOQKBQFFO"
      }
    ]
  }
}
{
  "result": {
    "Sequence": "2",
    "Operations": [
      {
        "Agreement": "ARJUFUYN4G625YLWWS6BDHXVWNKKQ6PMJWL6C35EE5SHDKY5VWB7OPE4",
        "Time": "2020-04-12T18:49:12Z",
        "Operation": "actioncall",
        "Action": "approved",
        "Tx": "LSEU62SQND7KFTHCJJFMT65ZUMJGDIOVO23PH5B3T7WZET57ED2A",
        "Root": "BACA6ZL7KKNDVJJ764WAAQ2UN5I5WSDV23H7DJAAXDF3C3MQHJSNV5U2",
        "Account": "ACPK4HPCM6HAAPAIE75ZJFSMNK2UI56CTCAAXNKI4THIQ33OOQKBQFFO"
      }
    ]
  }
}
```

## /api/v1/compensation-stats
API call to get summary of compensation by account address

**Parameters**
 - `AccountAddress`: The user account address to query, base32 encoded string.
 - `FromTime`: UTC time millis search window begin, Example: 1585967899000
 - `ToTime`: UTC time millis search window end, Example: 1585967899000

**Example**
```
curl --request GET \
  --url 'https://node.lonet:9179/api/v1/compensation-stats?AccountAddress=ACPK4HPCM6HAAPAIE75ZJFSMNK2UI56CTCAAXNKI4THIQ33OOQKBQFFO&FromTime=100&ToTime=1962663226436'
```

**Response**
```json
{
  "Dimensions": [
    {
      "Field": "account"
    },
    {
      "Field": "currency"
    }
  ],
  "Metrics": [
    {
      "Field": "amount",
      "DataType": "int64"
    }
  ],
  "Results": [
    {
      "Values": [
        "ACPK4HPCM6HAAPAIE75ZJFSMNK2UI56CTCAAXNKI4THIQ33OOQKBQFFO",
        "USD",
        "6400000000"
      ]
    }
  ]
}
```
