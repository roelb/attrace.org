---
title: Roots
---

Roots are the base data unit of the network, they are very light-weight state containers with conversion tracking state.
A Root can have an optional parent root and can be modified by calling actions on it through the agreement contract. A "click" is represented on the network by a single Root, where every "sale" or "lead" conversion is again represented by a child-root which can independently change status through action calls.

Every click is a root, every conversion is a child root. 

A root can change status and status-change history is preserved on the network.

The Root is a lightweight isolated state container, it's state is modified by executing Actions on it, which are specified in the contract.
A Root is created with a `type` (eg: click/lead/sale/...), which can't be modified after creation.
Roots allow parent-child relationships, where the tree of relationships increases cost of execution.

The Attrace network agrees on a pre-selected list of types and related statuses which have to be used.   

Usage of the correct actions will allow to join in performance statistics.

In the contract code, the types and statuses map to action calls.

Please check the contract library documentation to see what Roots can be created.

:::note
The root type max length is 20 characters.
:::note

<!-- ## Supported Root `type`s


| Type  | When to use it                                                       | What it does                                                                        | Allowed statuses               |
|-------|----------------------------------------------------------------------|-------------------------------------------------------------------------------------|--------------------------------|
| click | When a clickout or tap on a link happens by a customer               | Creates a new Root without a parent. The root hash is used as one-time tracking ID. | -                              |
| sale  | When a customer buys a product                                       | Creates a new Root with parent Root referred and executes sale logic.               | open, approved, rejected, paid |
| lead  | When a customer becomes contactable (email address/phone number/etc) | Creates a new Root with parent Root referred and executes lead logic.               | open, approved, rejected, paid |

## Supported root statuses for `lead` and `sale` types
Note: The root status max length is 20 characters.

In order to reward the publisher for the sale or lead it created, it needs to be confirmed and paid out by the advertiser. 
Therefore we define the following states for both sale and lead.

| State    | Description                                       |
|----------|---------------------------------------------------|
| open     | Initial state, newly created                      |
| approved | Sale has been marked as legit sale by advertiser  |
| rejected | Sale has been marked as invalid by advertiser     |
| paid     | The approved sale has been paid out by advertiser | -->
