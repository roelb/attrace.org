---
id: connectors
title: Connectors
slug: /connectors
---

Connectors are clients which implement the connectors.proto interface definition and provide rich integration with the parent platform.