---
title: Commandline Client (CLI)
---

## Install commandline client

In a normal workflow, we have an attrace executable installed to interact with different networks.

We use the `attr` cli client this, 

It can interact with the different networks, supports transferring value, creating/confirming agreements, staking nodes, ...

### Installation

#### MacOS

The installs the `attr` tool to `/usr/local/bin`.

```
mkdir -p /usr/local/bin && \
curl $(curl https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attr.darwin.txt) | gunzip > /usr/local/bin/attr && \
chmod +x /usr/local/bin/attr && \
xattr -d com.apple.quarantine /usr/local/bin/attr
```

##### Issue: command not found

The installation requires that your shell has `/usr/local/bin` on the $PATH variable. 
If this is not the case, follow this guide to update your $PATH variable:

1.  Edit your shell config file, one of these files usually in your home dir: `.zshrc`, `.zshenv`, `.bashrc`, `.bash_profile` or `.profile`.
2.  Append a line at the end:
    ```
    export PATH="/usr/local/bin:$PATH"
    ```
3.  Restart your terminals

##### Issue: permission denied

It might be that your folder is owned by the admin user.
You can install the attr tool as root by doing `sudo -s` and running the install command again.

#### Linux (amd64)
```
curl $(curl https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attr.linux-x64.txt) | gunzip > /usr/local/bin/attr && \
chmod +x /usr/local/bin/attr
```

#### Windows

You can find the latest file in:

```
https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attr.windows-x64.txt
```

### Verify installation

Running the command without arguments should show help output:

```
attr
```

## Running commands from your account

The executable requires a configured wallet.

We are transitioning to a system where keychain is used for managing accounts securely.

### Configure your local keychain

```
attr account set
// <-- it will ask for a key, paste your private key here.
```

## Example `attr` usage: 

Here we create a transaction which:

- has a TransferValue operation which instructs the network to move 0.0001 ATTR from your account to ADDR_BENEFICIARY account.
- is signed by the primary account using the private key loaded from the `wallet.yaml`.
- is broadcasted to the `betanet` (live network).

The output will show the generated tx hash, which can be used to look up processing status in the block explorer.

It requires sufficient funds on your account to successfully complete.

```
attr -n betanet transfer --to=ADDR_BENEFICIARY --attr=0.0001
```

## Deprecated: old system (using wallet.yaml file instead of keychain)

This requires a `wallet.yaml` file in the current working directory.
Without it will fail and cannot sign transactions or interact with nodes.

#### Create a wallet

First move to the directory first where you want to create the `wallet.yaml`

```
attr wallet generate
```

You now have a file `wallet.yaml` containing your address and private key.

**Secure your wallet**

```
chmod 600 wallet.yaml
```

_Important: Never share this with anybody. Store it safely in a secure location and make sure you have a backup somewhere._

_Important: There is no way to recover a lost wallet._
