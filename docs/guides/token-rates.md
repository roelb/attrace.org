---
title: Token Rate Conversion
hide_table_of_contents: true
---

The Attrace network supports working with other currencies and conversion from/to for example USD.

Conversion from/to another external currency in the world of crypto, requires 1 or more trusted parties which report the conversion rates to the network. These we commonly refer to as 'Oracles'.

## Accessing token rates

Important: the rates are stored multiplied by 1000000 (so we keep 6 digits accuracy).

**Through HTTP API on any public node:**
```
curl -s https://publicnodeurl/api/v1/accounts/{AccountAddress}/token-rates
```

*Example using the Attrace oracle:*
```
curl -s https://peer-drqanrmc06.testnet.attrace.com/api/v1/accounts/AC2W5LHJLW4MGMPLHCLDSJAGV7Q3SFC4FFZUB6N47S667KIFHODUKH6V/token-rates | jq
{
  "UpdateTime": "1578371185",
  "Rates": [
    {
      "Currency": "USD",
      "Rate": "10000"
    }
  ]
}

// The 10000 in this example reflects that a single ATTR token converts to 0.01 USD (=10000/1000000).
```

In one of the upcoming releases, these values will be accessible in the agreement contracts as well.

## Attrace Token Rate Oracle

The Attrace project provides an Oracle on all the networks which is reporting token rates and allows querying the token rates.

The oracle uses stable, multiple sources and smoothens sudden valuation changes of the ATTR token to/from USD to provide a usable stable conversion to work with.

The Attrace Token Rates can be found on address: `AC2W5LHJLW4MGMPLHCLDSJAGV7Q3SFC4FFZUB6N47S667KIFHODUKH6V`


## Custom Token Rates

Since the Attrace Token Rate Oracle is just one implementation of the oracle, others can also report token valuation to the network.

If you prefer to use another conversion rate system, or another currency, your own conversion Oracle can be implemented as well.
