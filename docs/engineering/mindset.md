---
title: Engineering Mindset
sidebar_label: Mindset
---

The Attrace network consists of:

-   Static number of elected public nodes who produce blocks (witnesses). We refer to those using `WN`.
-   Other public nodes (incl. witness candidates). We refer to those using `PN`.
-   Index nodes to serve dashboard queries. We refer to those using `IN`.

The network is in continuous development and receives updates regularly.    


## Development mindset

-   Keep releases "cheap" and "efficient" in terms of effort. We avoid having to long-term plan releases.   
-   It's better to do multiple small incremental releases than single breaking massive releases.   
-   Releasing should be as automated as possible and require zero manual intervention. Nodes should always migrate.


## Single repository

-  "core" is the core golang codebase, where the witness, protocol and index node code resides.
-  Web UI's are stored externally, but some are baked into the builds at build time. The binaries then include UI's like explorer and wallet.
-  Docs you are looking at and are decentralized through git.


## Extending the protocol

We primarily use protobuf (v3) for serialization, since performance is good, the community is active around it, it's stable and has wide support in many languages.

Good data modeling is assumed important and we prefer to work with data structures which think ahead.   

We try to always achieve forward compatibility and support backward compatibility for 3 months.

A good read on schema compatibility can be found [here](https://docs.confluent.io/current/schema-registry/avro.html#summary) (different schema format, but the practices hold true).


## Upgrading blockchain nodes

We want to avoid having complexity in code. Therefore upgrades are planned ahead and binaries are compiled with current-version support only.

Currently upgrading witnesses involves an operator-sync we do in telegram.

In a future release this will be automated and handled by the network itself (upgrade proposal + majority commit), this is almost ready.


## Core functionality guarantees

Architecture of most components is that almost everything network-related has to be able to break without impacting core affiliate functionality for all accounts.

Under the hood components use event sourcing, rebroadcasting, data model designs, ... which allow to achieve this behavior.

In a nutshell: a lead which has been sent from a publisher to an advertiser will not be lost when the network is down, it will be processed whenever network recovers. 

You can test this by having no token to generate clicks anymore or block network access to everything and generate some new leads. Then cause some more havoc by restarting nodes. When network and nodes are back, everything will be processed.


## Checklist for promoting and pushing builds to the next network
Both on public and index nodes:

-  Use the {{ network }}/top tool to review core metrics
-  Ensure integration tests all pass
-  Check if runtime behavior is nominal
    -   Block processing is stable?
    -   No new errors/warnings?
    -   Check system stats for anomalies
        -   CPU usage is nominal?
        -   Mem usage is nominal?
        -   Go routines and open file descriptors behavior is nominal?
        -   Disk usage growth is nominal?
        -   Network activity is nominal?
-  Check if current version index nodes are stable
-  Check if previous version index nodes are stable
-  Re-verify any previous upgrade problems by resetting state
-  Do manual verification of full-flow (create->lead->sale) and verify in account UI.
