---
title: Builds
hide_table_of_contents: true
---

## Published builds
Project builds done by the Attrace Project can be found in this location:

Base URL structure:
```
https://s3.eu-central-1.amazonaws.com/attrace-builds/core/$BRANCH/$TAG_OR_COMMIT/
```

**Branch identifiers of interest:**

-  `develop`: stable (unreleased) code
-  `testnet`: code proposed to testnet
-  `betanet`: code proposed to betanet


**Binaries:**

-  `attr`: cli client, to interact with the network from the terminal
-  `attri`: index node runtime, indexes the blockchain for statistic queries
-  `attrw`: public node runtime, processes the blockchain
-  `attrs`: service runtime, simplifies standalone integrations
-  `attrjsonclient`: universal client runtime, used by the backend plugins across languages

## Info files to latest builds

These info files contain a URL which points to the latest build.

Since we tag by commit to have history of builds, these are the entrypoint for automation.

```
https://s3.eu-central-1.amazonaws.com/attrace-builds/core/$BRANCH/latest/$BINARY.$ARCH.txt
```

## Supported architectures

| architecture | `attr` cli client | `attrw` public node | `attri` index node | `attrjsonclient` client | `attrs` service box |
|--------------|-------------------|---------------------|--------------------|-------------------------|---------------------|
| linux-x64    | yes               | yes                 | yes                | yes                     | yes                 |
| darwin       | yes               | - not recommended   | yes                | yes                     | yes                 |
| windows-x64  | yes               | - not recommended   | yes                | yes                     | yes                 |


