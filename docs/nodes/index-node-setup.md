---
title: Index Node Setup
---

Index nodes process blocks and store the required for fast queries, for basic dashboarding.

## Index Node responsibility
-  Tail the block stream
-  Can be queried by Account UI's
-  Extract useful operations and store them for fast analysis
-  Keep the data store performant

## Setting up a node (Docker)
TODO

## Setting up a node (Linux)

### High-level TLDR;
-  SSH into a linux server
-  Install the attri binary
-  Configure service
-  Start the service

### 1.  SSH into a linux server
During development we primarily test with Ubuntu 18.04, so that's a good choice, but any Linux system will do.

### 2.  Install the `attri` runtime

Fetch a recent build.
```
curl $(curl https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attri.linux-x64.txt) | gunzip > attri
chmod +x attri
```

Verify if the build works on your architecture. When you see help output, the binary is OK.
```
./attri
# Should show help output
```

Move the binary to your local binaries so you can run it everywhere
```
sudo mv attri /usr/local/bin/attri
```

Make sure to grant the binary access to run on port 443 (default external port).
```
sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/attri
```

### 3.  Create working directory and move into it
```
sudo mkdir /var/opt/attrace
sudo chown $USER /var/opt/attrace
cd /var/opt/attrace
```

### 4.  Configure wallet
Restore your `wallet.yaml` file, if you already have one.

If you don't have one, generate one:
```
attri wallet generate
```

Secure the wallet
```
chmod 600 wallet.yaml
```

### 5.  Create attrace system user
```
sudo useradd -r -s /bin/false attrace
sudo chown attrace wallet.yaml
sudo chown attrace /var/opt/attrace
```

### 6.  Bump system limits
Only required if you want to support many network connections.

```
sudo -s

echo "" >> /etc/sysctl.conf
echo "fs.file-max = 2097152" >> /etc/sysctl.conf

echo "" >> /etc/security/limits.conf
echo "attrace    soft    nofile    1000000"  >> /etc/security/limits.conf
echo "attrace    hard    nofile    1048576" >> /etc/security/limits.conf
```

### 7.  Install clickhouse database server

Simple guide:
1.  Follow these steps: [clickhouse setup .deb](https://clickhouse.tech/docs/en/getting_started/install/#install-from-deb-packages)
2.  `service clickhouse-server start`

### 8.  Configure and start the node
1.  Use below template to create your service unit file.
    -   When using SSL autocert, make sure to have a good configuration (see [Node Configuration Docs](./node-configuration))
2.  Put your edited file in `/etc/systemd/system/attri.service`, use `sudo` and your favorite editor to create this file.
3.  See the [Node Configuration Docs](./node-configuration) for additional configuration settings.

Template:
```
[Unit]
Description=Attrace index node
After=network.target

[Service]
Type=simple
User=attrace
Group=attrace

WorkingDirectory=/var/opt/attrace
ExecStart=/usr/local/bin/attri start --ext-host=node-external-hostname.YOURdomain.com --port 443 --tls-policy=auto -n betanet 
Restart=always    
RestartSec=10

[Install]
WantedBy=multi-user.target
```

Enable the service at boot time
```
sudo systemctl daemon-reload
sudo systemctl start attri
sudo systemctl status attri
# Make sure the output is green or review any errors.
# More errors can be found in syslog `tail -n 10000 /var/log/syslog`
```

Now your attrace index node should be syncing with the network. 

Verify your endpoint. (The first call might be slow if using autocert, since it will setup ssl certificates.)
```
curl https://{{ext-host-domain}}/api/v1/node/status
# => should show json status response like `{"Candidate":false,"LagInSec":0,"PeerAddress":"your-ext-host:443","Witness":false}`
```

### 6.  Enable the service at boot time
```
sudo systemctl enable attri
```


##  System requirements index node
The Index Node itself is quite lightweight, it's just a process streaming blocks and sending queries.
The database we use (clickhouse), has good memory behavior and scales well with your data.

Start with the cheapest instance type you can possibly find, or run it on an existing webserver.

-  CPU: Depending on your traffic, at least 1 cpu core.
-  Memory: Depends on your traffic, having 512MB free memory should suffice for most use cases.
-  Disk: Depending on your traffic, good practice is to reserve at least 2 gigabytes.


##  Backups
You can backup the datastore according to it's best practices.
If you ever lose the data, there always is the option to rebuild.

##  Capacity planning
TODO: add curl call to index node to expose capacity stats for recent timeframe.


##  Monitoring
TODO: describe healthcheck route and stats reporting


## Updating index node

Example using latest builds:
```
sudo systemctl stop attri
curl $(curl https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attri.linux-x64.txt) | gunzip > attri
chmod +x attri && sudo mv attri /usr/local/bin/attri && sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/attri
sudo systemctl start attri
```
