---
title: Witness
hide_table_of_contents: true
---

Witnesses serve the network, by processing public transactions and generting blocks. 
In exchange for that, they receive the fees of the transactions they process.

The initial betanet operates with 13 active witnesses. 

## TLDR; Becoming a witness requires:
1.  Running a stable public node.
2.  Announcing to the network that you are electable.
3.  Getting the attrace community to stake your witness.
4.  Once you've received sufficient token holders to stake your witness, your node will automatically join the elected pool.

## Detailed 

### 1.  Running a full public node

Please follow [Setup Public Node Guide](./public-node-setup).

### 2.  Make your account electable

Make sure you have sufficient token value on your account.
```
cd ~/attrace
attrw -n betanet witness change-candidacy --candidacy=electable
```

### 3.  Collect community votes

You will need to motivate Attrace token holders to support your node. 

Having a decentralized network of high quality witnesses shared amongst partners in the industry is in the advantage of the token holders.

Incentives can be better performance, better stability, better security, being a good partner for the network and bringing in traffic, ...

Users can vote by replacing their existing votes, example for voting for 3 candidates and staking 1 ATTR token on them:
```
attrw -n betanet witness stake --candidates=AAFLTUOGS6GUIABD3KDSB6MYE6JICYR2RU6HS7SLLRHUTMUJGX2I7BMG,ACRHO2WWHEAJK2LGSRC2OHOCVXDBZSHF5MLEFJEA3X44YIMFGPWO6H6V,ACD7FPS7Q7XXUUQ6RIQBW7BE2LFPVOOIIQ3C6QJASE62ZRNHYZYSDAHA --value=1.0
``` 
