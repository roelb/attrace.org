---
title: Node configuration
hide_table_of_contents: true
---

You can use `attrw --help` or `attri --help` for help and drill into more detailed arguments using that.

Below are some more detailed guides on them ore advanced concepts.

## All node types

###  TLS settings

**Autocert using Let's Encrypt**

If your domain is below the Let's Encrypt limits, you can use auto ssl certification.

Make sure port 443 is reachable on the ext-host DNS!

```
--tls-policy=auto
```

**Use certificate and key files**

```
--tls-policy=file --tls-cert-file=/path/to/ssl/chain.pem --tls-key-file=/path/to/ssl/server.key
```

