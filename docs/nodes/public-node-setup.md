---
title: Public Node Setup
---

Public nodes process the public blockchain and have a copy of the network's public state.   

When candidate and elected by the network, they become witnesses and produce blocks. 

## Public Node responsibilities
-  Wallet UI (https://$nodeUrl/wallet)
-  Block Explorer UI (https://$nodeUrl/network)
-  Block store/sync
-  State store/sync
-  Transaction processing
-  Block production, if node is witness elected

## Setting up a node.

### High-level TLDR;
-  SSH into a linux server
-  Configure system
-  Fetch and restore a recent state snapshot
-  Verify genesis and tip blocks
-  Configure the service
-  Start the service

### 1.  SSH into a linux server
During development we primarily test with Ubuntu 18.04, so that's a good choice, but any other Linux should do.

### 2.  Install the `attrw` runtime

Fetch a recent build.
```
curl $(curl https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attrw.linux-x64.txt) | gunzip > attrw
chmod +x attrw
```

Verify if the build works on your architecture. When you see help output, the binary is OK.
```
./attrw
```

Move the binary to your local binaries so you can run it everywhere
```
sudo mv attrw /usr/local/bin/attrw
```

Make sure to grant the binary access to run on port 443 (default external port).
```
sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/attrw
```

### 3.  Create working directory and move into it
```
sudo mkdir /var/opt/attrace
sudo chown $USER /var/opt/attrace
cd /var/opt/attrace
```

### 4.  Configure wallet
The wallet contains the account and private key. The `PrimaryAccount` in the file is used by the daemon.

Restore your `wallet.yaml` file, if you already have one.

If you don't have one, generate one:
```
attrw wallet generate
```

Secure the wallet
```
chmod 600 wallet.yaml
```

Put a copy of the wallet in your homedir, so you can easily interact with the network easily.
```
mkdir -p ~/attrace
sudo cp wallet.yaml $HOME/attrace/wallet.yaml
sudo chown $USER $HOME/attrace/wallet.yaml
chmod 400 ~/attrace/wallet.yaml
```

### 5.  Create attrace system user
```
sudo useradd -r -s /bin/false attrace
sudo chown attrace wallet.yaml
sudo chown attrace /var/opt/attrace
```

### 6.  Bump system and attrace limits
Witnesses have to support many network connections.

Note: only require for public nodes which want to become witnesses.

```
sudo -s

echo "" >> /etc/sysctl.conf
echo "fs.file-max = 2097152" >> /etc/sysctl.conf

echo "" >> /etc/security/limits.conf
echo "attrace    soft    nofile    1000000"  >> /etc/security/limits.conf
echo "attrace    hard    nofile    1048576" >> /etc/security/limits.conf
```

### 7.  Setup and create empty Mysql database
Make sure to install mysql first. Attrace was tested against MySQL v5.7.

Make CLI connection to mysql as root user
```
mysql [-u root] [-p]
```

Create user and database, edit accordingly for your setup.   
Make sure to replace `__PASSWORD__` in below code.
```
create user if not exists attrace@'localhost' identified with mysql_native_password by '__PASSWORD__';
drop database if exists attrace;
create database if not exists attrace CHARACTER SET utf8 COLLATE utf8_general_ci;
grant all on attrace.* to attrace@'localhost';
```

### 8.  Restore a recent snapshot of the blockchain DB

Install `zstd`, we use the zstandard for compression, for better performance.   
```
sudo apt-get update -y
sudo apt-get install -y zstd
```

**Get the latest snapshot**
```
# For testnet
curl -O $(curl https://s3.eu-central-1.amazonaws.com/blocks.testnet.attrace.com/exports/AAFLTUOGS6GUIABD3KDSB6MYE6JICYR2RU6HS7SLLRHUTMUJGX2I7BMG/latest.txt)

# For betanet
curl -O $(curl https://s3.eu-central-1.amazonaws.com/blocks.betanet.attrace.com/exports/AD2EGWK35FBPFE26ALQ5GO7PBN2AMPEGQVM4UYWAUK4V3RQD5FCJEDYN/latest.txt)
```

Check the filename in current working directory, we will refer to this file later on as `$SNAPSHOT`
```
ls *.zstd
```

**Restore $SNAPSHOT**

In both examples, replace $SNAPSHOT with the file you've downloaded.   

Update your mysql client command as required: `mysql databasename`, optional `-u $username` to select user, optional `-p` to ask for a password. If you have access denied, make sure to use the right user/password or none at all.

a.   Slower version (without mysql tweaks)

```
zstdcat $SNAPSHOT.zstd | mysql -u attrace -p attrace
```

b.   Faster version (with mysql tweaks)

   1.  Create file `pre.sql`  with contents:
        ```
        SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT, AUTOCOMMIT = 0;
        SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
        SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
        ```

   2.  Create file `post.sql` with contents:
        post.sql:
        ```
        SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
        SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
        SET AUTOCOMMIT = @OLD_AUTOCOMMIT;
        ``` 

   3.  Import with pre and post files:

       ```
       cat <(cat pre.sql) <(zstdcat $SNAPSHOT.zstd) <(cat post.sql) | mysql -u attrace -p attrace
       ```

### 9.  Verify successful import
Verify if the tip of the chain is correct according the export page.
```
mysql -u attrace -p attrace -e 'select * from chain_state'
```

Verify the genesis block is correct:
```
mysql -u attrace -p attrace -e 'select hash from blocks where sequence_id = 1'
```

If this checks out, your snapshot can be trusted since genesis block and the tip of the chain are correct.

Now that the snapshot is restored, you can delete the original snapshot file.

### 10.  Configure and start the node
1.  Use below template to create your service unit file.
    -   When using SSL autocert, make sure to have a good configuration (see [Node Configuration Docs](./node-configuration))
2.  Put your edited file in `/etc/systemd/system/attrw.service`, use `sudo` and your favorite editor to create this file.
3.  See the [Node Configuration Docs](./node-configuration) for additional configuration settings.

Template:
```
[Unit]
Description=Attrace public node
After=network.target

[Service]
Type=simple
User=attrace
Group=attrace

WorkingDirectory=/var/opt/attrace
ExecStart=/usr/local/bin/attrw start --ext-host={{ eg: node-hostname.yourdomain.com }} --port 443 -n betanet --tls-policy=auto
Restart=always    
RestartSec=10

# You can override the mysql config through env settings, these are the defaults.
# If the database is missing and the user has permissions, the DB will be created.
# Environment=MYSQL_HOST=localhost
# Environment=MYSQL_PORT=3306
# Environment=MYSQL_DATABASE=attrace
# Environment=MYSQL_USER=attrace
# Environment=MYSQL_PASSWORD=attrace

[Install]
WantedBy=multi-user.target
```

Enable the service at boot time
```
sudo systemctl daemon-reload
sudo systemctl start attrw
sudo systemctl status attrw
# Make sure the output is green or review any errors.
# More errors can be found in syslog `tail -n 10000 /var/log/syslog`
```

Now your attrace public node should be syncing with the network. 

Verify your endpoint. (The first call might be slow if using autocert, since it will setup ssl certificates.)
```
curl https://{{ext-host-domain}}/api/v1/node/status
# => should show json status response like `{"Candidate":true,"LagInSec":0,"PeerAddress":"your-ext-host:443","Witness":false}`
```

### 11.  Enable the service at boot time
```
sudo systemctl enable attrw
```


### 12.  Verify NTP or time sync service

If your node will join the public network, it's important a time service is active which keeps time somewhat correct.

Verify that NTP or similar are running on your system.


##  System requirements public node
The instances should not be heavy, the code is quite efficient.

Witnesses should avoid using burstable instances, or monitor really well to avoid hitting credit limits.

-  CPU: We recommend a minimum of 2 virtual cpu cores.
-  Memory: We recommend 4GB minimal.
-  Disk: At time of writing, the testnet blockchain is uncompressed (with indexes) around 26GB.

Over time it won't be required for full nodes to have a full copy of the complete blockchain, but for now they have.


##  Capacity planning
TODO: add curl call to witnesses to expose capacity stats for recent timeframe.


##  Becoming witness
Please see the detailed guide on [Witness Nodes](./witness)


## Updating public node

Example using latest builds:
```
sudo systemctl stop attrw
curl $(curl https://s3.eu-central-1.amazonaws.com/attrace-builds/core/betanet/latest/attrw.linux-x64.txt) | gunzip > attrw
chmod +x attrw && sudo mv attrw /usr/local/bin/attrw && sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/attrw
sudo systemctl start attrw
```

##  Additional monitoring
The Attrace project actively monitors elected witnesses, if they allow collecting system and process metrics.   
Both are optional, but very informative to good operation of the network.

For witness node operators, it's appreciated to support this in the early stages of live network, since this helps debugging/improving the network.

**Open firewall:**

-   IP to grant access to below ports: `3.126.36.116` (/32).   
-   Ports to open on your firewall: `9100`, `9256`.

**Software to install:**

-   prometheus-node-exporter
    -   Ubuntu example:   
        -   `sudo apt update && sudo apt install prometheus-node-exporter`
-   process-exporter
    -   Find releases here: [https://github.com/ncabatoff/process-exporter/releases](https://github.com/ncabatoff/process-exporter/releases)
    -   Ubuntu example: 
        1.  First download most recent amd64 .deb from the releases
        2.  `sudo dpkg -i $filename`
