---
title: Overview
hide_table_of_contents: true
---

The Attrace has two kinds of nodes: `Public Nodes` and `Index Nodes`.

## Public Node

A public node syncs the public blockchain and can serve queries. 

Through election they can become witnesses and they join in production of blocks.

### Applications served by the Public Node

The public nodes listen for TLS connections, by default on port 443. 

| Mount path | Protocol  | Application                                                                                                                   |
|------------|-----------|-------------------------------------------------------------------------------------------------------------------------------|
| -          | grpc      | GRPC Listener                                                                                                                 |
| /api/v1    | http + ws | The REST API to interface with the blockchain. Also serves web socket connections.                                            |
| /api-docs  | http      | Swagger API docs and API playground.                                                                                          |
| /wallet    | http      | The last version of Wallet UI at build time of the binary, which allows interfacing with your account to transfer token.      |
| /explorer  | http      | The last version of the Explorer UI at build time of the binary, which allows browsing the public network and the blockchain. |

## Index Node

Index Nodes are lightweight nodes indexing the blockchain for performant dashboard aggregation queries.

### Applications served by the Index Node

The index nodes listen for TLS connections, by default on port 443. 

| Mount path | Protocol  | Application                         |
|------------|-----------|-------------------------------------|
| -          | grpc      | GRPC Listener                       |
| /api/v1    | http + ws | The REST API                        |
| /api-docs  | http      | Swagger API docs and API playground |
