# Attrace project docs

THE AUDIENCE OF THIS SITE ARE PEOPLE WHO DON'T KNOW WHAT ATTRACE IS OR HOW IT RELATES TO ATTRACE.ORG, ASSUME NOTHING: ZERO-KNOWLEDGE!!

GUIDE THEM, TELL LOGICAL STORIES, FOLLOW LOGICAL FLOW.

EXPLAIN AS IF THE READER IS A GOLDEN RETRIEVER.

#
 
## Developing docs

-  Docusaurus v2 docs: https://v2.docusaurus.io/docs/
-  Styling framework: https://facebookincubator.github.io/infima/docs/getting-started/introduction
-  Read: https://v2.docusaurus.io/docs/docs-introduction

### Installation

```console
yarn install
```

### Local Development

```console
yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

### Build

```console
yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

```console
GIT_USER=<Your GitHub username> USE_SSH=true yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.
