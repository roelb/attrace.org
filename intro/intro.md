---
id: intro
title: Introduction
slug: /
---

:::note
On this page you will find a short description of the core components and concepts.  
Use the menu's to go skip ahead into more detailed documentation.
:::note

## Getting started

Attrace is a set of technologies which leverage the blockchain to build new and improved affiliate solutions. Solutions which increase trust and transparency, lower cost, build a reputation-driven ecosystem to lower fraud and increase user privacy, allow for more automation and new payment flows between publisher, advertiser, consultant or any other party involved.

-  [Attrace Software Stack](#software-stack) (Connectors & libraries, Witness nodes, Index nodes)
-  [Attrace Concepts](#concepts) (Agreements & contracts, Accounts, Roots)


## Software Stack

### Connectors

Connectors are backend modules which are installed into the websites of publishers and advertisers. They are fully integrated with the developer framework (eg: wordpress) and with the Attrace blockchain. They deliver an optimal affiliate experience which enforces trust and transparency best practices.  
We support many platforms: Wordpress, WooCommerce, PHP, and others. 
A serverless solution is available for websites which have no backend.  

### Witness nodes

Witnesses are the heart of the network. They process transactions with operations and create new blocks which modify the state of the network. Blocks are validated and need to be accepted by the majority of the network before they become part of the blockchain. The network can be trusted because nodes are geographically and organizationally distributed and no single entity can have majority control. Node operators earn ATTR token for their services. Anybody can become a witness operator by getting a fast node elected.  

### Index nodes

Index nodes provide aggregated data and allow you to build reports directly on the network without having to operate your own server.
When you have high-volume contracts, you can run your own index node or rent one of the network nodes to index your traffic.
The get you started, attrace.org runs a fair-use index node with a 4-month data retention window.


## Concepts

### Agreements

Agreements are light-weight contract configurations which define the rules under which certain publishers, advertisers and consultants agree to work together. Agreements are recorded on the blockchain and digitally signed by all involved parties.

### Contracts

Contracts are dynamic light-weight applications which execute workflows and payment rules as defined in the agreements. They are well-vetted, optimized and high-performant applications. Contract code has to be accepted by the network majority before it can be used. Once accepted, the contract will remain operational and are under long term support, so traffic is never lost.

### Accounts

An Account represents a private-public keypair which is accepted by the network. The Account address is created from the public key, usually encoded in base32 format, creating a 56-character string. Private keys can digitally sign transactions and through the public account address, the network can verify if transactions were signed by the right account.
An account holds ATTR token.

### Roots

Roots are the base data unit of the network, they are very light-weight state containers with conversion tracking state.
A Root can have an optional parent root and can be modified by calling actions on it through the agreement contract. A "click" is represented on the network by a single Root, where every "sale" or "lead" conversion is again represented by a child-root which can independently change status through action calls.


## Networks

Attrace has multiple networks active. 

### Betanet

Betanet is the nickname for the live network on which the majority of witnesses agree, it consists at the time of writing of 9 active witnesses.

Only code which has been found very stable is proposed to the betanet. Witnesses decide if they run the new code or not.

-  Explore the blockchain on one of the nodes: [Block Explorer](http://explorer.attrace.com/)
-  Latest discovery manifest: [Discovery Manifest](https://s3.eu-central-1.amazonaws.com/discovery.attrace.com/betanet/manifest.json)


### Testnet

Testnet is the nickname for a 7-node network on which the project proposes new builds which are expected stable with new features at a rapid pace.

-  Explore the blockchain on one of the nodes: [Block Explorer](http://explorer.testnet.attrace.com/)
-  Latest discovery manifest: [Discovery Manifest](https://s3.eu-central-1.amazonaws.com/discovery.attrace.com/testnet/manifest.json)

