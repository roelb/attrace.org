import React from 'react';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './styles.module.css';
import Link from '@docusaurus/Link';

function ContractLibrary() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;

  return (
    <Layout
      title={`Contract Library - ${siteConfig.title}`}
      description="Documentation on the contracts">
      <main className={styles.main}>
        <div className={styles.pageSection}>
          <h2>TODO: contract logic + library + fetch contracts from network + details on roots</h2>

          <Link className={styles.editLink} to={siteConfig.customFields.baseEditUrl+'/src/pages/index.js'}># Edit this page</Link> 
        </div>
      </main>
    </Layout>
  );
}

export default ContractLibrary;
