import React from 'react';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './styles.module.css';
import Link from '@docusaurus/Link';

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;

  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Attrace blockchain network documentation">
      <main className={styles.main}>
        <div className={styles.pageSection}>
          <h2>Welcome</h2>

          <p>Right now you are reading the Attrace blockchain technical docs.</p>

          <p>These docs are written for network consultants, core engineers, connector and integration developers, node operators or anybody interested in how the Attrace network and blockchain operates at a lower level.</p>

          <div className="alert alert--primary" role="alert" style={{ marginBottom: '1rem' }}>
            Visit <a href="https://attrace.com">attrace.com</a> if you are looking for the live affiliate network instead (which has been built on top of the Attrace blockchain).
          </div>

          <p>These docs are open source and constantly evolving, so if you can’t find what you’re looking for or have ideas for improvements, please contribute by filing a GitLab issue or merge request in this repository. You can find an edit link on the bottom of every page.</p>

          <h2>Introduction</h2>

          <p>Attrace is a set of technologies which leverage the blockchain to build new and improved affiliate solutions. Solutions which increase trust and transparency, lower cost, build a reputation-driven ecosystem to lower fraud and increase user privacy, allow for more automation and new payment flows between publisher, advertiser, consultant or any other party involved.</p>

          <h2>Getting Started</h2>

          <p>
          On these pages you can find a quick introduction to some key concepts of the network.
          </p>

          <ul>
            <li><Link to="intro/#software-stack">Attrace Software Stack</Link>
              <ul>
                <li>Connectors &amp; libraries</li>
                <li>Witness nodes</li>
                <li>Index nodes</li>
              </ul>
            </li>
            <li><Link to="intro/#concepts">Attrace Concepts</Link>
              <ul>
                <li>Agreements &amp; contracts</li>
                <li>Accounts</li>
                <li>Roots</li>
              </ul>
            </li>
          </ul>

          <Link className={styles.editLink} to={siteConfig.customFields.baseEditUrl+'/src/pages/index.js'}># Edit this page</Link> 
        </div>
      </main>
    </Layout>
  );
}

export default Home;


          // {/* <p>Below you will find a short introduction to some key concepts of the network, ignore and skip ahead using the menu to navigate to various sections.</p>

          // <h2>Attrace software stack</h2>

          // <h3>Connectors</h3>
          // <p>
          //   Connectors are backend modules which are installed into the websites of publishers and advertisers. They integrate with the Attrace blockchain easily and deliver an optimal affiliate experience which enforces trust and transparency best practices. A serverless solution is available for serverless websites. We support many platforms: Wordpress, WooCommerce, PHP, NodeJS, Golang, ...

          //   <Link className={styles.moreLink} to="connectors/">More info</Link>
          // </p>

          // <h3>Witness nodes (public nodes)</h3>
          // <p>
          //   Witnesses are the heart of the network. They process transactions with operations and create new blocks which modify the state of the network. Blocks are validated and need to be accepted by the majority of the network before they become part of the blockchain. The network can be trusted because nodes are geographically and organizationally distributed and no single entity can have majority control. Node operators earn ATTR token for their services. Anybody can become a witness operator by getting a fast node elected.  
            
          //   <Link className={styles.moreLink} to="nodes/">More info</Link>
          // </p> 

          // <h3>Index nodes</h3>
          // <p>
          //   Index nodes provide aggregated data and allow you to build reports directly on the network without having to operate your own server.
          //   When you have high-volume contracts, you can run your own index node or rent one of the network nodes to index your traffic.
          //   <div style={{ marginTop: '1em' }}>The get you started, attrace.org runs a fair-use index node with a 4-month data retention window.</div>

          //   <Link className={styles.moreLink} to="nodes/">More info</Link>
          // </p>

          // <h2>Concepts</h2>

          // <h3>Agreements</h3>
          // <p>
          //   Agreements are light-weight contract configurations which define the rules under which certain publishers, advertisers and consultants agree to work together. Agreements are recorded on the blockchain and digitally signed by all involved parties.

          //   <Link className={styles.moreLink} to="docs/">More info</Link>
          // </p>

          // <h3>Contracts</h3>
          // <p>
          //   Contracts are dynamic light-weight applications which execute workflows and payment rules as defined in the agreements. They are well-vetted, optimized and high-performant applications. Contract code has to be accepted by the network majority before it can be used. Once accepted, the contract will remain operational and are under long term support, so traffic is never lost.

          //   <Link className={styles.moreLink} to="contracts/">More info</Link>
          // </p>

          // <h3>Accounts</h3>
          // <p>
          //   An Account represents a private-public keypair which is accepted by the network. The Account address is created from the public key, usually encoded in base32 format, creating a 56-character string. Private keys can digitally sign transactions and through the public account address, the network can verify if transactions were signed by the right account.
          //   An account holds ATTR token.

          //   <Link className={styles.moreLink} to="contracts/">More info</Link>
          // </p>

          // <h3>Roots</h3>
          // <p>
          //   Roots are the base data unit of the network, they are very light-weight state containers with conversion tracking state.
          //   A Root can have an optional parent root and can be modified by calling actions on it through the agreement contract. A "click" is represented on the network by a single Root, where every "sale" or "lead" conversion is again represented by a child-root which can independently change status through action calls.

          //   <Link className={styles.moreLink} to="contracts/">More info</Link>
          // </p> */}
